package bot

import (
	"gopkg.in/tucnak/telebot.v3"
	"interpol-notices/storage"
	"time"
)

type Bot struct {
	Storage *storage.Storage
	Bot     *telebot.Bot
}

func NewBot(token string, storage *storage.Storage) (*Bot, error) {
	var err error
	b := Bot{Storage: storage}

	b.Bot, err = telebot.NewBot(telebot.Settings{
		Token:     token,
		Poller:    &telebot.LongPoller{Timeout: 10 * time.Second},
		ParseMode: "html",
	})

	if err != nil {
		return nil, err
	}

	return &b, nil
}

func (b *Bot) Start() {
	b.Bot.Start()
}

func (b *Bot) Stop() error {
	return b.Stop()
}
