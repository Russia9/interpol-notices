package app

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/go-co-op/gocron"
	"github.com/imroc/req"
	"github.com/rs/zerolog/log"
	"go.mongodb.org/mongo-driver/mongo"
	"gopkg.in/tucnak/telebot.v3"
	"interpol-notices/bot"
	"interpol-notices/entity"
	"interpol-notices/storage"
	"interpol-notices/storage/mongodb"
	"interpol-notices/utils"
	"io/ioutil"
	"os"
	"regexp"
	"strconv"
	"strings"
	"time"
)

type App struct {
	Bot       *bot.Bot
	Channel   *telebot.Chat
	Storage   storage.Storage
	Resources map[string]map[string]string
	Scheduler *gocron.Scheduler
}

func NewApp() (*App, error) {
	log.Debug().Msg("Creating new app")
	var err error
	var app App

	// Resources
	log.Trace().Msg("Resolving resources")
	res, err := ioutil.ReadFile("resources.json")
	if err != nil {
		return nil, err
	}
	err = json.Unmarshal(res, &app.Resources)
	if err != nil {
		return nil, err
	}

	// Create Mongo
	log.Trace().Msg("Connecting to Mongo")
	c, _ := context.WithTimeout(context.Background(), time.Second*10)
	app.Storage, err = mongodb.NewStorage(c, os.Getenv("MONGO_URI"), os.Getenv("MONGO_DB"))
	if err != nil {
		return nil, err
	}

	// Create Scheduler
	log.Trace().Msg("Creating Scheduler")
	app.Scheduler = gocron.NewScheduler(time.UTC)
	_, err = app.Scheduler.Every(5).Minutes().Do(app.UpdateNotices)
	if err != nil {
		return nil, err
	}

	// Create Bot
	log.Trace().Msg("Creating Bot")
	app.Bot, err = bot.NewBot(os.Getenv("TELEGRAM_TOKEN"), &app.Storage)
	if err != nil {
		return nil, err
	}

	log.Trace().Msg("Getting notifications channel")
	channelID, err := strconv.ParseInt(os.Getenv("TELEGRAM_CHANNEL"), 10, 64)
	if err != nil {
		return nil, err
	}

	app.Channel, err = app.Bot.Bot.ChatByID(channelID)
	if err != nil {
		return nil, err
	}

	return &app, nil
}

func (a *App) Start() {
	log.Debug().Msg("Starting App")
	a.Scheduler.StartAsync()
	a.Bot.Start()
}

func (a *App) Stop() error {
	log.Debug().Msg("Stopping App")
	a.Scheduler.Stop()
	return a.Bot.Stop()
}

func (a *App) UpdateNotices() {
	log.Debug().Msg("Updating Notices")
	log.Trace().Msg("Getting Notices from API")
	get, err := req.Get("https://ws-public.interpol.int/notices/v1/red", req.QueryParam{
		"resultPerPage": "160",
	})
	if err != nil {
		log.Error().Str("module", "update").Err(err).Send()
		return
	}

	type request struct {
		Embedded map[string]*[]*entity.Notice `json:"_embedded"`
	}

	r := request{}

	log.Trace().Msg("Unmarshalling JSON")
	err = json.Unmarshal(get.Bytes(), &r)
	if err != nil {
		log.Error().Str("module", "update").Err(err).Send()
		return
	}

	if r.Embedded != nil {
		if r.Embedded["notices"] != nil {
			for _, n := range *r.Embedded["notices"] {
				_, err = a.Storage.GetNotice(context.Background(), n.ID)
				if errors.Is(err, mongo.ErrNoDocuments) {
					get, err = req.Get(fmt.Sprintf("https://ws-public.interpol.int/notices/v1/red/%s", strings.Replace(n.ID, "/", "-", -1)))
					if err != nil {
						log.Warn().Str("module", "update").Err(err).Send()
						continue
					}

					var notice entity.Notice
					err = json.Unmarshal(get.Bytes(), &notice)
					if err != nil {
						log.Warn().Str("module", "update").Err(err).Send()
						continue
					}

					text := strings.Builder{}

					if len(notice.Warrants) != 0 {
						text.WriteString(
							fmt.Sprintf(
								"Wanted by <b>%s</b>:\n\n",
								a.Resources["countries"][notice.Warrants[0].IssuedBy],
							),
						)
					}

					emoji := ""
					if notice.Nationalities != nil {
						emoji = utils.GetFlag((*notice.Nationalities)[0]) + " "
					}
					text.WriteString(fmt.Sprintf("<b>%s%s, %s</b>\n", emoji, notice.LastName, notice.FirstName))

					if notice.Height != 0 {
						text.WriteString(fmt.Sprintf("<b>Height:</b> <code>%.1f</code>\n", notice.Height))
					}
					if notice.Weight != 0 {
						text.WriteString(fmt.Sprintf("<b>Weight:</b> <code>%.1f</code>\n", notice.Weight))
					}
					if len(notice.HairsColors) != 0 {
						text.WriteString(fmt.Sprintf("<b>Hair:</b> <code>%s</code>\n", a.Resources["hair"][notice.HairsColors[0]]))
					}
					if len(notice.EyesColors) != 0 {
						text.WriteString(fmt.Sprintf("<b>Eyes:</b> <code>%s</code>\n", a.Resources["eyes"][notice.EyesColors[0]]))
					}

					if len(notice.Languages) != 0 {
						text.WriteString("<b>Languages:</b> ")
						for i, lang := range notice.Languages {
							if i != 0 {
								text.WriteString(", ")
							}
							text.WriteString(a.Resources["language"][lang])
						}
						text.WriteString("\n")
					}

					if notice.BirthPlace != "" || notice.BirthDate != "" || notice.BirthCountry != "" {
						text.WriteString("<b>Birth:</b> ")
						i := 0

						if notice.BirthDate != "" {
							text.WriteString(notice.BirthDate)
							i += 1
						}

						if notice.BirthPlace != "" {
							if i != 0 {
								text.WriteString(", ")
							}
							text.WriteString(notice.BirthPlace)
							i += 1
						}

						if notice.BirthCountry != "" {
							if i != 0 {
								text.WriteString(", ")
							}
							text.WriteString(a.Resources["countries"][notice.BirthCountry])
							i += 1
						}
						text.WriteString("\n")
					}
					text.WriteString("\n")

					if len(notice.Warrants) != 0 {
						text.WriteString(fmt.Sprintf("<b>Wanted for:</b>\n%s", notice.Warrants[0].Charge))
					}

					if notice.Links["thumbnail"] != nil {
						l := notice.Links["thumbnail"]["href"]
						parsed := regexp.MustCompile("(.*/)(\\d+)$").FindAllStringSubmatch(l, -1)
						id, err := strconv.Atoi(parsed[0][2])
						if err != nil {
							return
						}

						_, err = a.Bot.Bot.Send(a.Channel, &telebot.Photo{
							File:    telebot.FromURL(parsed[0][1] + strconv.Itoa(id-1)),
							Caption: text.String(),
						})
						if err != nil {
							log.Error().Err(err).Send()
							continue
						}
					}

					err = a.Storage.CreateNotice(context.Background(), notice)
					if err != nil {
						log.Warn().Str("module", "update").Err(err).Send()
						continue
					}
				}
			}
		}
	}
}
