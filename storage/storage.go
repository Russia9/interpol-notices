package storage

import (
	"context"
	"interpol-notices/entity"
)

type Storage interface {
	GetNotice(c context.Context, id string) (*entity.Notice, error)
	CreateNotice(c context.Context, notice entity.Notice) error
	EditNotice(c context.Context, notice *entity.Notice) error
	DeleteNotice(c context.Context, id string) error
}
