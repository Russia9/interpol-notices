package mongodb

import (
	"context"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
	"interpol-notices/entity"
	"interpol-notices/storage"
	"time"
)

type Mongo struct {
	Client *mongo.Client
	DB     *mongo.Database
}

func NewStorage(c context.Context, uri string, database string) (storage.Storage, error) {
	var err error
	var m Mongo

	m.Client, err = mongo.Connect(c, options.Client().ApplyURI(uri))
	if err != nil {
		return nil, err
	}

	err = m.Client.Ping(c, readpref.Primary())
	if err != nil {
		return nil, err
	}

	m.DB = m.Client.Database(database)

	return &m, nil
}

func (m *Mongo) GetNotice(c context.Context, id string) (*entity.Notice, error) {
	res := m.DB.Collection("notices").FindOne(c, bson.M{"_id": id})
	if res.Err() != nil {
		return nil, res.Err()
	}

	var result entity.Notice
	err := res.Decode(&result)
	if err != nil {
		return nil, err
	}

	return &result, nil
}

func (m *Mongo) CreateNotice(c context.Context, notice entity.Notice) error {
	notice.CreatedAt = time.Now()
	notice.UpdatedAt = time.Now()
	_, err := m.DB.Collection("notices").InsertOne(c, notice)
	return err
}

func (m *Mongo) EditNotice(c context.Context, notice *entity.Notice) error {
	notice.UpdatedAt = time.Now()
	return m.DB.Collection("notices").FindOneAndReplace(c, bson.M{"_id": notice.ID}, notice).Err()
}

func (m *Mongo) DeleteNotice(c context.Context, id string) error {
	return m.DB.Collection("notices").FindOneAndDelete(c, bson.M{"_id": id}).Err()
}
