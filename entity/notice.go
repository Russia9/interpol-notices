package entity

import "time"

type Notice struct {
	ID string `json:"entity_id" bson:"_id"`

	Warrants  []Warrant `json:"arrest_warrants,omitempty"`
	FirstName string     `json:"forename,omitempty"`
	LastName  string     `json:"name,omitempty"`
	Gender    string     `json:"sex_id,omitempty"`

	BirthDate    string `json:"date_of_birth,omitempty"`
	BirthPlace   string `json:"place_of_birth,omitempty"`
	BirthCountry string `json:"country_of_birth_id,omitempty"`

	Nationalities *[]string `json:"nationalities,omitempty"`

	Height float64 `json:"height,omitempty"`
	Weight float64 `json:"weight,omitempty"`

	EyesColors  []string `json:"eyes_colors,omitempty"`
	HairsColors []string `json:"hairs_colors,omitempty"`

	DistinguishingMarks string `json:"distinguishing_marks,omitempty"`

	Languages []string `json:"languages_spoken_ids,omitempty"`

	Links map[string]map[string]string `json:"_links,omitempty"`

	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updated_at"`
}
