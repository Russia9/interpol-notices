package entity

type Warrant struct {
	IssuedBy          string `json:"issuing_country_id"`
	Charge            string `json:"charge"`
	ChargeTranslation string `json:"charge_translation"`
}
