package utils

func GetFlag(code string) string {
	if len(code) != 2 {
		return ""
	}
	return string(rune(code[0])+127397) + string(rune(code[1])+127397)
}
